import { Injectable } from '@angular/core';
import { Observable } from "rxjs/Observable";
import { of } from "rxjs/observable/of";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { catchError, map, tap } from "rxjs/operators";

import { Album } from "./album";
import { MessageService } from "./message.service";

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class AlbumService {

  private albumsUrl = 'https://dev.admin.observantrecords.com/api/v1/album';

  constructor(
      private http: HttpClient,
      private messageService: MessageService
  ) { }

  getAlbums(): Observable<Album[]> {
    return this.http.get<Album[]>(this.albumsUrl).pipe(
        tap( albums => this.log("fetched albums.") ),
        catchError(this.handleError<any>('updateArtist'))
    );
  }

    getAlbum(id: number): Observable<Album> {
        const url = `${this.albumsUrl}/${id}`;
        return this.http.get<Album>(url).pipe(
            tap( _ => this.log(`fetched album id=${id}`) ),
            catchError( this.handleError<Album>(`getAlbum id=${id}`) )
        );
    }

    private log(message: string) {
        this.messageService.add('ArtistService: ' + message);
    }

    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
    private handleError<T> (operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // TODO: better job of transforming error for user consumption
            this.log(`${operation} failed: ${error.message}`);

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }
}
