export class Artist {
    artist_id: number;
    artist_last_name: string;
    artist_first_name?: string;
    artist_display_name?: string;
    artist_alias?: string;
    artist_url?: string;
    artist_bio?: string;
    artist_bio_more?: string;
    created_at?: Date;
    updated_at?: Date;
    deleted_at?: Date;
}