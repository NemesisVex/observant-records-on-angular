import { Artist } from "./artist";

export class Album {
    album_id: number;
    album_artist_id: number;
    album_primary_release_id?: number;
    album_format_id?: number;
    album_ctype_locale?: string;
    album_title: string;
    album_alias?: string;
    album_image?: string;
    album_is_visible?: boolean;
    album_release_date?: Date;
    album_order?: number;
    created_at?: Date;
    updated_at?: Date;
    deleted_at?: Date;
    artist?: Artist;
}