import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ArtistsComponent } from "./artists/artists.component";
import { ArtistDetailComponent } from "./artist-detail/artist-detail.component";
import { ArtistEditComponent } from "./artist-edit/artist-edit.component";
import {ArtistDeleteComponent} from "./artist-delete/artist-delete.component";

const routes: Routes = [
    { path: '', redirectTo: '/artists', pathMatch: 'full' },
    { path: 'artists', component: ArtistsComponent },
    { path: 'artists/artist/add', component: ArtistEditComponent },
    { path: 'artists/artist/:id', component: ArtistDetailComponent },
    { path: 'artists/artist/:id/edit', component: ArtistEditComponent },
    { path: 'artists/artist/:id/delete', component: ArtistDeleteComponent }
]

@NgModule({
    exports: [ RouterModule ],
    imports: [ RouterModule.forRoot(routes) ]
})
export class AppRoutingModule {}