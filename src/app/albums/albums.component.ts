import { Component, OnInit } from '@angular/core';

import { Album } from "../album";
import { AlbumService } from "../album.service";

@Component({
  selector: 'app-albums',
  templateUrl: './albums.component.html',
  styleUrls: ['./albums.component.css']
})
export class AlbumsComponent implements OnInit {

  albums: Album[];

  selectedAlbum: Album;

  constructor(private albumService: AlbumService) { }

  ngOnInit() {
    this.getAlbums()
  }

  getAlbums() {
    this.albumService.getAlbums().subscribe(albums => this.albums = albums);
  }

}
