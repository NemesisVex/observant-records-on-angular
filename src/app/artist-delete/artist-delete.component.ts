import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { Location } from "@angular/common";

import { Artist } from "../artist";
import { ArtistService } from "../artist.service";

@Component({
  selector: 'app-artist-delete',
  templateUrl: './artist-delete.component.html',
  styleUrls: ['./artist-delete.component.css']
})
export class ArtistDeleteComponent implements OnInit {

    @Input() artist: Artist;

    constructor(
        private route: ActivatedRoute,
        private artistService: ArtistService,
        private location: Location
    ) { }

  ngOnInit() {
      this.getArtist();
  }

    getArtist(): void {
        const id = +this.route.snapshot.paramMap.get('id');
        this.artistService.getArtist(id).subscribe(artist => this.artist = artist);
    }

    goBack(): void {
        this.location.back();
    }

    delete(artist: Artist): void {
        this.artistService.deleteArtist(this.artist).subscribe(() => this.goBack());
    }
}
