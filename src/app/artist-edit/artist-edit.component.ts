import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { Location } from "@angular/common";

import { Artist } from "../artist";
import { ArtistService } from "../artist.service";

@Component({
  selector: 'app-artist-edit',
  templateUrl: './artist-edit.component.html',
  styleUrls: ['./artist-edit.component.css']
})
export class ArtistEditComponent implements OnInit {

    @Input() artist: Artist;

    constructor(
        private route: ActivatedRoute,
        private artistService: ArtistService,
        private location: Location
    ) {}

  ngOnInit() {
      const id = +this.route.snapshot.paramMap.get('id');
      if (id > 0) {
          this.getArtist();
      } else {
          this.artist = new Artist;
      }
  }

    getArtist(): void {
        const id = +this.route.snapshot.paramMap.get('id');
        this.artistService.getArtist(id).subscribe(artist => this.artist = artist);
    }

    goBack(): void {
        this.location.back();
    }

    save(): void {
        const id = +this.route.snapshot.paramMap.get('id');
        if (id > 0) {
            this.update();
        } else {
            this.create();
        }
    }

    create(): void {
        this.artistService.addArtist(this.artist)
            .subscribe(() => this.goBack());
    }

    update(): void {
        this.artistService.updateArtist(this.artist)
            .subscribe(() => this.goBack());
    }
}
