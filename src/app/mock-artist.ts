import { Artist} from "./artist";

export const ARTISTS: Artist[] = [
    { artist_id: 1, artist_last_name: 'Eponymous 4', artist_display_name: 'Eponymous 4', artist_alias: 'eponymous-4' },
    { artist_id: 2, artist_last_name: 'Empty Ensemble', artist_display_name: 'Empty Ensemble', artist_alias: 'empty-ensemble' },
    { artist_id: 3, artist_last_name: 'Penzias and Wilson', artist_display_name: 'Penzias and Wilson', artist_alias: 'penzias-and-wilson' }
];