import { Injectable } from '@angular/core';
import { Observable } from "rxjs/Observable";
import { of } from "rxjs/observable/of";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { catchError, map, tap } from "rxjs/operators";

import { Artist } from "./artist";
import { MessageService } from "./message.service";

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class ArtistService {

  private artistsUrl = 'https://dev.admin.observantrecords.com/api/v1/artist';

  constructor(
      private http: HttpClient,
      private messageService: MessageService
  ) { }

  getArtists(): Observable<Artist[]> {
    return this.http.get<Artist[]>(this.artistsUrl).pipe(
      tap(artists => this.log("fetched artists.") ),
      catchError(this.handleError('getArtists', []))
    );
  }

  getArtist(id: number): Observable<Artist> {
    const url = `${this.artistsUrl}/${id}`;
    return this.http.get<Artist>(url).pipe(
      tap( _ => this.log(`fetched artist id=${id}`) ),
      catchError( this.handleError<Artist>(`getArtist id=${id}`) )
    );
  }

  addArtist (artist: Artist): Observable<any> {
      return this.http.post(this.artistsUrl, artist, httpOptions).pipe(
          tap(_ => this.log(`added artist id=${artist.artist_id}`)),
          catchError(this.handleError<any>('addArtist'))
      );
  }

  updateArtist (artist: Artist): Observable<any> {
    return this.http.put(this.artistsUrl, artist, httpOptions).pipe(
      tap(_ => this.log(`updated artist id=${artist.artist_id}`)),
      catchError(this.handleError<any>('updateArtist'))
    );
  }

  deleteArtist (artist: Artist | number): Observable<Artist> {
      const id = typeof artist === 'number' ? artist : artist.artist_id;
      const url = `${this.artistsUrl}/${id}`;

      console.log("id is " + id);
      return this.http.delete<Artist>(url, httpOptions).pipe(
          tap(_ => this.log(`deleted artist id=${id}`)),
          catchError(this.handleError<Artist>('deleteArtist'))
      );
  }

  private log(message: string) {
      this.messageService.add('ArtistService: ' + message);
  }

    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
    private handleError<T> (operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // TODO: better job of transforming error for user consumption
            this.log(`${operation} failed: ${error.message}`);

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }
}
